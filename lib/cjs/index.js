"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const uuid_1 = __importDefault(require("./utils/uuid"));
const is_extension_1 = __importDefault(require("./utils/is-extension"));
const is_mobile_1 = __importDefault(require("./utils/is-mobile"));
const VERSION = '0.0.27';
const TWETCH_URL = 'https://twetch.com';
const actionMap = {};
const request = (action, params = {}) => {
    return new Promise((res, rej) => {
        const actionId = (0, uuid_1.default)();
        const actionUrl = {
            CONNECT: '/connect',
            ABI: '/approve'
        }[action];
        const popup = window.open(`${TWETCH_URL}${actionUrl}?origin=${window.origin}&actionId=${actionId}`);
        actionMap[actionId] = { res, rej, popup, abi: action === 'ABI' && params };
    });
};
const TwetchWeb3 = {
    isTwetch: true,
    connect: () => __awaiter(void 0, void 0, void 0, function* () {
        if ((0, is_extension_1.default)()) {
            return window.bitcoin.connect();
        }
        if ((0, is_mobile_1.default)()) {
            return request('CONNECT', {});
        }
        if (!window.bitcoin) {
            return window.open('https://twetch.com/downloads', '_blank');
        }
    }),
    abi: (params) => __awaiter(void 0, void 0, void 0, function* () {
        if ((0, is_extension_1.default)()) {
            return window.bitcoin.abi(params);
        }
        if ((0, is_mobile_1.default)()) {
            return request('ABI', params);
        }
        if (!window.bitcoin) {
            return window.open('https://twetch.com/downloads', '_blank');
        }
    }),
    publicKey: null,
    paymail: null,
    isConnected: false,
    version: VERSION
};
if (typeof window !== 'undefined') {
    window.addEventListener('message', (e) => {
        var _a;
        const message = e.data && e.data.message;
        if (!message) {
            return;
        }
        const action = actionMap[message.actionId];
        if (!action) {
            return;
        }
        switch (message.action) {
            case 'APPROVE_CONNECT':
                TwetchWeb3.publicKey = message.publicKey;
                TwetchWeb3.paymail = message.paymail;
                TwetchWeb3.isConnected = true;
                action.res({ paymail: message.paymail, publicKey: message.publicKey });
                break;
            case 'REQUEST_ABI':
                (_a = action === null || action === void 0 ? void 0 : action.popup) === null || _a === void 0 ? void 0 : _a.postMessage({
                    message: {
                        action: 'REQUEST_ABI_SUCCESS',
                        actionId: message.actionId,
                        abi: action.abi
                    }
                }, TWETCH_URL);
                break;
            case 'APPROVE_ABI':
                action.res(message);
                break;
            case 'REJECT_ABI':
                action.rej();
                break;
            default:
                action.res();
                break;
        }
    });
}
exports.default = TwetchWeb3;
//# sourceMappingURL=index.js.map