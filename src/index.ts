import uuid from './utils/uuid'
import isExtension from './utils/is-extension'
import isMobile from './utils/is-mobile'

const VERSION = '0.0.27'
const TWETCH_URL = 'https://twetch.com'

const actionMap: {
	[key: string]: {
		res: (data?: any) => void
		rej: () => void
		popup: Window | null
		abi: boolean
	}
} = {}

const request = (action: string, params: any = {}) => {
	return new Promise((res, rej) => {
		const actionId = uuid()
		const actionUrl = {
			CONNECT: '/connect',
			ABI: '/approve'
		}[action]

		const popup = window.open(
			`${TWETCH_URL}${actionUrl}?origin=${window.origin}&actionId=${actionId}`
		)

		actionMap[actionId] = { res, rej, popup, abi: action === 'ABI' && params }
	})
}

const TwetchWeb3 = {
	isTwetch: true,
	connect: async (): Promise<any> => {
		if (isExtension()) {
			return window.bitcoin.connect()
		}

		if (isMobile()) {
			return request('CONNECT', {})
		}

		if (!window.bitcoin) {
			return window.open('https://twetch.com/downloads', '_blank')
		}
	},
	abi: async (params: any): Promise<any> => {
		if (isExtension()) {
			return window.bitcoin.abi(params)
		}

		if (isMobile()) {
			return request('ABI', params)
		}

		if (!window.bitcoin) {
			return window.open('https://twetch.com/downloads', '_blank')
		}
	},
	publicKey: null,
	paymail: null,
	isConnected: false,
	version: VERSION
}

if (typeof window !== 'undefined') {
	window.addEventListener('message', (e) => {
		const message = e.data && e.data.message

		if (!message) {
			return
		}

		const action = actionMap[message.actionId]

		if (!action) {
			return
		}

		switch (message.action) {
			case 'APPROVE_CONNECT':
				TwetchWeb3.publicKey = message.publicKey
				TwetchWeb3.paymail = message.paymail
				TwetchWeb3.isConnected = true
				action.res({ paymail: message.paymail, publicKey: message.publicKey })
				break
			case 'REQUEST_ABI':
				action?.popup?.postMessage(
					{
						message: {
							action: 'REQUEST_ABI_SUCCESS',
							actionId: message.actionId,
							abi: action.abi
						}
					},
					TWETCH_URL
				)
				break
			case 'APPROVE_ABI':
				action.res(message)
				break
			case 'REJECT_ABI':
				action.rej()
				break
			default:
				action.res()
				break
		}
	})
}

export default TwetchWeb3
