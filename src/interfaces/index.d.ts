export {}

interface InjectedBitcoin {
	isTwetch: boolean
	connect: (props?: { onlyIfTrusted?: boolean }) => Promise<void>
	abi: (params: any) => Promise<any>
}

declare global {
	interface Window {
		bitcoin: InjectedBitcoin
	}
}
