declare const TwetchWeb3: {
    isTwetch: boolean;
    connect: () => Promise<any>;
    abi: (params: any) => Promise<any>;
    publicKey: null;
    paymail: null;
    isConnected: boolean;
    version: string;
};
export default TwetchWeb3;
