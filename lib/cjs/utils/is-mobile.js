"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
function isMobile() {
    return /Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent);
}
exports.default = isMobile;
//# sourceMappingURL=is-mobile.js.map