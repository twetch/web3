function isExtension() {
    var _a;
    return (_a = window === null || window === void 0 ? void 0 : window.bitcoin) === null || _a === void 0 ? void 0 : _a.isTwetch;
}
export default isExtension;
//# sourceMappingURL=is-extension.js.map