[Documentation](README.md) / Exports

# Documentation

## Table of contents

### Variables

- [default](modules.md#default)

## Variables

### default

• `Const` **default**: `Object`

#### Type declaration

| Name | Type |
| :------ | :------ |
| `abi` | (`params`: `any`) => `Promise`<`any`\> |
| `connect` | () => `Promise`<`any`\> |
| `isConnected` | `boolean` |
| `isTwetch` | `boolean` |
| `paymail` | ``null`` |
| `publicKey` | ``null`` |
| `version` | `string` |

#### Defined in

[index.ts:33](https://gitlab.com/twetch/web3/-/blob/74e4c9e/src/index.ts#L33)
