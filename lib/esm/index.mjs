import uuid from './utils/uuid.mjs';
import isExtension from './utils/is-extension.mjs';
import isMobile from './utils/is-mobile.mjs';
const VERSION = '0.0.27';
const TWETCH_URL = 'https://twetch.com';
const actionMap = {};
const request = (action, params = {}) => {
    return new Promise((res, rej) => {
        const actionId = uuid();
        const actionUrl = {
            CONNECT: '/connect',
            ABI: '/approve'
        }[action];
        const popup = window.open(`${TWETCH_URL}${actionUrl}?origin=${window.origin}&actionId=${actionId}`);
        actionMap[actionId] = { res, rej, popup, abi: action === 'ABI' && params };
    });
};
const TwetchWeb3 = {
    isTwetch: true,
    connect: async () => {
        if (isExtension()) {
            return window.bitcoin.connect();
        }
        if (isMobile()) {
            return request('CONNECT', {});
        }
        if (!window.bitcoin) {
            return window.open('https://twetch.com/downloads', '_blank');
        }
    },
    abi: async (params) => {
        if (isExtension()) {
            return window.bitcoin.abi(params);
        }
        if (isMobile()) {
            return request('ABI', params);
        }
        if (!window.bitcoin) {
            return window.open('https://twetch.com/downloads', '_blank');
        }
    },
    publicKey: null,
    paymail: null,
    isConnected: false,
    version: VERSION
};
if (typeof window !== 'undefined') {
    window.addEventListener('message', (e) => {
        var _a;
        const message = e.data && e.data.message;
        if (!message) {
            return;
        }
        const action = actionMap[message.actionId];
        if (!action) {
            return;
        }
        switch (message.action) {
            case 'APPROVE_CONNECT':
                TwetchWeb3.publicKey = message.publicKey;
                TwetchWeb3.paymail = message.paymail;
                TwetchWeb3.isConnected = true;
                action.res({ paymail: message.paymail, publicKey: message.publicKey });
                break;
            case 'REQUEST_ABI':
                (_a = action === null || action === void 0 ? void 0 : action.popup) === null || _a === void 0 ? void 0 : _a.postMessage({
                    message: {
                        action: 'REQUEST_ABI_SUCCESS',
                        actionId: message.actionId,
                        abi: action.abi
                    }
                }, TWETCH_URL);
                break;
            case 'APPROVE_ABI':
                action.res(message);
                break;
            case 'REJECT_ABI':
                action.rej();
                break;
            default:
                action.res();
                break;
        }
    });
}
export default TwetchWeb3;
//# sourceMappingURL=index.js.map